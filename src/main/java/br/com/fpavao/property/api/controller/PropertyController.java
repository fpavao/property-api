package br.com.fpavao.property.api.controller;

import br.com.fpavao.property.api.dto.PropertiesDTO;
import br.com.fpavao.property.api.model.Property;
import br.com.fpavao.property.api.model.Province;
import br.com.fpavao.property.api.repository.PropertyRepository;
import br.com.fpavao.property.api.repository.ProvinceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.Set;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @author Fabio E. Pavão
 * @since 1.0 - 11/06/17
 */
@RestController
@RequestMapping("/properties")
public class PropertyController {
	
	static final Logger LOG = LoggerFactory.getLogger(PropertyController.class);
	
	private PropertyRepository propertyRepository;
	private ProvinceRepository provinceRepository;
	
	@Autowired
	public PropertyController(PropertyRepository repository,
	                          ProvinceRepository provinceRepository) {
		this.propertyRepository = repository;
		this.provinceRepository = provinceRepository;
	}
	
	@RequestMapping(method = POST)
	public ResponseEntity<?> add(@Valid @RequestBody Property property, UriComponentsBuilder uriBuilder) {
		
		for (Province province : provinceRepository.findByLocation(property.getX(), property.getY())) {
			property.getProvinces().add(province.getName());
		}
		
		propertyRepository.save(property);
		
		LOG.info("StatusCode: {}, RequestURI: {}, Message: Propriedade criada com sucesso [ID {}]",
			201, "/properties/", property.getId());
		
		return ResponseEntity
			.created(uriBuilder.path("/properties/{id}").buildAndExpand(property.getId()).toUri())
			.body(property);
	}
	
	@RequestMapping(value = "/{id}", method = GET)
	public ResponseEntity<?> get(@PathVariable Long id) {
		
		Property property = propertyRepository.findOne(id);
		
		if (property == null) {
			LOG.info("StatusCode: {}, RequestURI: {}, Message: Propriedade não foi encontrada [ID {}]",
				404, "/properties/", id);
			return ResponseEntity.notFound().build();
		} else {
			LOG.info("StatusCode: {}, RequestURI: {}, Message: Propriedade encontrada [ID {}]", 200,
				"/properties/", id);
			return ResponseEntity.ok(property);
		}
	}
	
	@RequestMapping(params = {"ax", "ay", "bx", "by"}, method = GET)
	public ResponseEntity<?> search(@RequestParam("ax") int ax, @RequestParam("ay") int ay,
	                                @RequestParam("bx") int bx, @RequestParam("by") int by) {
		
		Set<Property> properties = propertyRepository.findByArea(ax, ay, bx, by);
		
		if (properties.isEmpty()) {
			LOG.info("StatusCode: {}, RequestURI: {}, Message: Nenhuma propriedade foi encontrada [Parametros ax={}&ay={}&bx={}&by={}]",
				404, "/properties/", ax, ay, bx, by);
			return ResponseEntity.notFound().build();
		} else {
			LOG.info("StatusCode: {}, RequestURI: {}, Message: {} Propriedades encontradas.",
				201, "/properties/", properties.size());
			return ResponseEntity.ok(new PropertiesDTO(properties));
		}
	}
	
}

package br.com.fpavao.property.api.repository;

import br.com.fpavao.property.api.model.Property;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Set;

/**
 *
 * Interface responsável pelas operações de banco de dados
 * específicas para a entidade {@link Property}.
 *
 * <p>Estende a interface padrão {@link MongoRepository} do Spring Data,
 * que expõem recursos de persistência e operações de CRUD,
 * ajudando o desenvolvedor com as operações mais comuns.</p>
 *
 * @author Fabio E. Pavão
 * @since 1.0 - 11/06/17
 */
public interface PropertyRepository extends MongoRepository<Property, Long> {
	
	@Query("{ $and: [ { x: { $gte: ?0, $lte: ?2} }, { y: { $lte: ?2, $gte: ?3} } ] }")
	Set<Property> findByArea(int ax, int ay, int bx, int by);
	
}

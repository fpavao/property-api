package br.com.fpavao.property.api.model;

import br.com.fpavao.property.api.DefaultConfigTest;
import org.hibernate.validator.HibernateValidator;
import org.junit.Before;
import org.junit.Test;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Fabio E. Pavão
 * @since 1.0 - 13/06/17
 */
public class PropertyTest extends DefaultConfigTest {
	
	private LocalValidatorFactoryBean validator;
	
	@Before
	public void setup() {
		
		validator = new LocalValidatorFactoryBean();
		validator.setProviderClass(HibernateValidator.class);
		validator.afterPropertiesSet();
	}
	
	@Test
	public void shouldSuccessfullyValidate() {
		
		Property property = new Property(
			6L,"Imóvel código 6, com 5 quartos e 4 banheiros.", 1673000,
			"Sit ea commodo magna culpa qui officia proident. Nostrud esse consectetur adipisicing duis sunt commodo reprehenderit quis nulla irure.",
			1363,122,5,4,167);
		
		Set<ConstraintViolation<Property>> constraintViolations = validator.validate(property);
		assertTrue(constraintViolations.isEmpty());
	}
	
	@Test
	public void bedsOverTheFiveLimit() {
		
		Property property = new Property(
			6L,"Imóvel código 6, com 5 quartos e 4 banheiros.", 1673000,
			"Sit ea commodo magna culpa qui officia proident. Nostrud esse consectetur adipisicing duis sunt commodo reprehenderit quis nulla irure.",
			1363,122,6,4,167);
		
		Set<ConstraintViolation<Property>> constraintViolations = validator.validate(property);
		
		assertEquals( 1, constraintViolations.size() );
		assertEquals("deve ser menor ou igual a 5", constraintViolations.iterator().next().getMessage());
	}
	
	@Test
	public void bathsOverTheFourLimit() {
		
		Property property = new Property(
			6L,"Imóvel código 6, com 5 quartos e 4 banheiros.", 1673000,
			"Sit ea commodo magna culpa qui officia proident. Nostrud esse consectetur adipisicing duis sunt commodo reprehenderit quis nulla irure.",
			1363,122,5,5,167);
		
		Set<ConstraintViolation<Property>> constraintViolations = validator.validate(property);
		
		assertEquals( 1, constraintViolations.size() );
		assertEquals("deve ser menor ou igual a 4", constraintViolations.iterator().next().getMessage());
	}
	
	@Test
	public void squareMetersOverThe240Limit() {
		
		Property property = new Property(
			6L,"Imóvel código 6, com 5 quartos e 4 banheiros.", 1673000,
			"Sit ea commodo magna culpa qui officia proident. Nostrud esse consectetur adipisicing duis sunt commodo reprehenderit quis nulla irure.",
			1363,122,5,4,241);
		
		Set<ConstraintViolation<Property>> constraintViolations = validator.validate(property);
		
		assertEquals( 1, constraintViolations.size() );
		assertEquals("deve ser menor ou igual a 240", constraintViolations.iterator().next().getMessage());
	}
	
	@Test
	public void bedsBelowTheOneLimit() {
		
		Property property = new Property(
			6L,"Imóvel código 6, com 5 quartos e 4 banheiros.", 1673000,
			"Sit ea commodo magna culpa qui officia proident. Nostrud esse consectetur adipisicing duis sunt commodo reprehenderit quis nulla irure.",
			1363,122,0,4,167);
		
		Set<ConstraintViolation<Property>> constraintViolations = validator.validate(property);
		
		assertEquals( 1, constraintViolations.size() );
		assertEquals("deve ser maior ou igual a 1", constraintViolations.iterator().next().getMessage());
	}
	
	@Test
	public void bathsBelowTheOneLimit() {
		
		Property property = new Property(
			6L,"Imóvel código 6, com 5 quartos e 4 banheiros.", 1673000,
			"Sit ea commodo magna culpa qui officia proident. Nostrud esse consectetur adipisicing duis sunt commodo reprehenderit quis nulla irure.",
			1363,122,5,0,167);
		
		Set<ConstraintViolation<Property>> constraintViolations = validator.validate(property);
		
		assertEquals( 1, constraintViolations.size() );
		assertEquals("deve ser maior ou igual a 1", constraintViolations.iterator().next().getMessage());
	}
	
	@Test
	public void squareMetersBelowThe20Limit() {
		
		Property property = new Property(
			6L,"Imóvel código 6, com 5 quartos e 4 banheiros.", 1673000,
			"Sit ea commodo magna culpa qui officia proident. Nostrud esse consectetur adipisicing duis sunt commodo reprehenderit quis nulla irure.",
			1363,122,5,4,19);
		
		Set<ConstraintViolation<Property>> constraintViolations = validator.validate(property);
		
		assertEquals( 1, constraintViolations.size() );
		assertEquals("deve ser maior ou igual a 20", constraintViolations.iterator().next().getMessage());
	}
	
	@Test
	public void outsideSpotipposLimits() {
		
		Property property = new Property(
			6L,"Imóvel código 6, com 5 quartos e 4 banheiros.", 1673000,
			"Sit ea commodo magna culpa qui officia proident. Nostrud esse consectetur adipisicing duis sunt commodo reprehenderit quis nulla irure.",
			1401,1001,5,4,21);
		
		Set<ConstraintViolation<Property>> constraintViolations = validator.validate(property);
		
		assertEquals( 2, constraintViolations.size() );
	}
	
}

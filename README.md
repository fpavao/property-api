# Imóveis em Spotippos

Olá! Seja bem vindo à Web API de imóveis em Spotippos.

Aqui você poderá inserir imóveis, consultar um imóvel específico ou consultar os imóveis dentro de uma determinada área. 

## Arquitetura
Para essa projeto foram utilizadas tecnologias e frameworks que auxiliam a implementação de padrões de projetos e boas práticas de desenvolvimento de software. Aumentam a produtividade, fornecendo as operações mais comuns, por exemplo, configuração de conexão com banco de dados, configuração de servlets, http listeners, e até mesmo um servidor de aplicação _embedded._
 
Escolhi o já famoso, MongoDB, pela sua facilidade em se trabalhar com o padrão JSON, pelo poder de queries com expressões, cache nativo, escalabilidade entre outros. E no caso de evolução, suporte a geolocalização. 

Abaixo lista de frameworks e tecnologias utilizadas:

<ul>
<li>Java 8</li>
<li>Spring Boot</li>
<li>Spring Data</li>
<li>Embedded MongoDB</li>
<li>Spring Test</li>
<li>JUnit</li>
<li>Hamcrest</li>
<li>Maven</li>
</ul>

## Quick Start
Após clonar o repositório, para iniciar a aplicação basta digitar: **mvn spring-boot:run.**

A aplicação estará disponível em **http://localhost:8080/properties.**

Para a validação desse projeto, a base de dados é carregada na inicialização da aplicação.

### Crie imóveis em Spotippos

Para a criação de um imóvel, a api espera a estrutura abaixo. 

- Todos os campos são obrigatório
- A área total de Spotippos é definida da seguinte forma `0 <= x <= 1400` e `0 <= y <= 1000`
- No máximo 5 quartos (beds), e no mínimo 1
- No máximo 4 banheiros (baths), e no mínimo 1
- No máximo 240 metros quadrados, e no mínimo 20

#### Request:
```
POST /properties
```

Body:
```json
{ 
  "id": 9999, 
  "title": "Imóvel código 1, com 3 quartos e 2 banheiros.",
   "price": 643000, 
   "description": "Laboris quis quis elit commodo eiusmod qui exercitation. In laborum fugiat quis minim occaecat id.", 
   "x": 1257, 
   "y": 928, 
   "beds": 4, 
   "baths": 2, 
   "squareMeters": 61 
}
```

#### Response:

**Imóvel criado com sucesso**

Header
- HTTP/1.1 201
- Location: /properties/9999
- Content-Type: application/json;charset=UTF-8


Body:
```json
{
   "id" : 9999,
   "title" : "Imóvel código 1, com 3 quartos e 2 banheiros.",
   "price" : 643000,
   "description" : "Laboris quis quis elit commodo eiusmod qui exercitation. In laborum fugiat quis minim occaecat id.",
   "y" : 928,
   "x" : 1257,
   "beds" : 4,
   "baths" : 2,
   "squareMeters" : 61,
   "provinces" : ["Jaby"]
}
```

**Imóvel com informação inválida**

Header
- HTTP/1.1 400
- Content-Type: application/json;charset=UTF-8

Body:
```json
{
   "error" : "Bad Request",
   "path" : "/properties/",
   "status" : 400,
   "message" : "Validation failed for object='property'. Error count: 1",
   "errors" : [
      {
         "objectName" : "property",
         "rejectedValue" : 6,
         "defaultMessage" : "deve ser menor ou igual a 5",
         "codes" : ["Max.property.beds","Max.beds","Max.java.lang.Integer","Max"],
         "arguments" : [{"codes" : ["property.beds","beds"], "defaultMessage" : "beds","code" : "beds","arguments" : null},5],
         "field" : "beds",
         "code" : "Max",
         "bindingFailure" : false
      }
   ],
   "exception" : "org.springframework.web.bind.MethodArgumentNotValidException",
   "timestamp" : 1497610840716
}
```

### Busque um imóvel específico em Spotippos

Busque um imóvel específico a partir de seu `id`.

#### Request:
```
  GET /properties/{id}
```

#### Response:

**Imóvel encontrado**

Header:
- HTTP/1.1 200
- Content-Type: application/json;charset=UTF-8

Body:

```json
{
   "id" : 14,
   "title" : "Imóvel código 14, com 4 quartos e 3 banheiros.",
   "price" : 802000,
   "y" : 939,
   "x" : 580,
   "beds" : 4,
   "baths" : 3,
   "description" : "Mollit fugiat voluptate ipsum proident. Fugiat laboris anim et culpa tempor excepteur reprehenderit.",
   "squareMeters" : 80,
   "provinces" : ["Gode","Ruja"]
}
```

**Imóvel não encontrado**

Header:
- HTTP/1.1 404

Body:

Sem informações.


### Busque imóveis em Spotippos

Dada uma área identificada pelos pelos pontos A e B, onde A é o ponto superior esquerdo e B é o ponto inferior direito, a api busca os imóveis que estão contidos nessa área. Cada ponto é representado pelas cordenadas `x` e `y`. O ponto A é representado por `ax` e `ay` e B por `bx` e `by`.


#### Request:
```
  GET /properties?ax={integer}&ay={integer}&bx={integer}&by={integer}
```

#### Response:

**Imóveis encontrados**

Header:
- HTTP/1.1 200
- Content-Type: application/json;charset=UTF-8

Body:
```json
{
  "totalProperties":353,
  "properties":[
    {
      "id":89,
      "title":"Imóvel código 89, com 5 quartos e 4 banheiros.",
      "price":1242000,
      "description":"Cillum proident et ex nisi dolor Lorem sit non incididunt. Ut adipisicing sunt sint velit ea sint quis et ullamco eu incididunt.",
      "x":12,
      "y":537,
      "beds":5,
      "baths":4,
      "squareMeters":122,
      "provinces":["Gode"]
    },
    { 
      "id":116,
      "title":"Imóvel código 116, com 3 quartos e 2 banheiros.",
      "price":833000,
      "description":"Aliqua id fugiat aliqua excepteur aliqua. Excepteur ea aute labore ipsum exercitation commodo aute irure enim cupidatat cillum et irure anim.",
      "x":116,
      "y":538,
      "beds":3,
      "baths":2,
      "squareMeters":79,
      "provinces":["Gode"]},
    {
      "id":119,
      "title":
      "Imóvel código 119, com 2 quartos e 1 banheiros.",
      "price":511000,
      "description":"Elit ipsum incididunt proident reprehenderit non veniam labore voluptate. Exercitation esse reprehenderit voluptate et voluptate qui adipisicing nostrud sit occaecat non irure.",
      "x":133,
      "y":574,
      "beds":2,
      "baths":1,
      "squareMeters":48,
      "provinces":["Gode"]
    },
    {"..."},
    {"..."}
  ]
}
```

**Nenhum imóvel não encontrado**

Header:
- HTTP/1.1 404

Body:

Sem informações.

## Próximos passos

- Containerização (Docker).
- Proxy, NGINX, por exemplo, com módulo WAF.
- Autenticação utlizando padrão OAuth2 ou OpenID;
- Autorização por perfis a nível de método ou ACL;
- Otimização do processo de consulta de Províncias;
- Otimização do processo de consulta de Propriedades;
- Melhoria no tratamento de exceçoes;
- Testes de integração;
- Testes de aceitação.